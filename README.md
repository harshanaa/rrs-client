# rrs-client
Restaurent reservation system consist 2 projects. This is the React Native based client implementation.

## Run Locally

This requires a running server instance. Run [rrs-server](https://bitbucket.org/harshanaa/rrs-server/src/master/) first. Follow the instructions there to get the server up and running.

Find your local [IP address](http://osxdaily.com/2010/11/21/find-ip-address-mac/). 

If you're running on a device, make sure its connected to the same network.

To clone and run this project, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. Also you will need XCode to run IOS app and Android SDK to run Android app. From your command line:  
  
  .
```bash
# Clone this repository
git clone https://harshanaa@bitbucket.org/harshanaa/rrs-client.git
# Go into the repository
cd rrs-client
# Install dependencies
npm install or yarn add
```
.
  
Goto ```rrs-client/src/constants/index.js``` and add your local IP address. Eg: if your ip is ```192.168.8.100```, add ```http://192.168.8.100```.  
.
  
```bash
# Run the app 
react-native run-ios or react-native run-android
```
