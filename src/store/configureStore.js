import { createStore, applyMiddleware, compose } from 'redux';
import { AsyncStorage } from 'react-native';
import thunk from 'redux-thunk';
import {
  persistStore,
  persistCombineReducers,
} from 'redux-persist';

import persistConfig from './reduxPersistConfig';
import reducers from '../reducers';

export default function configureStore() {

  const rootReducer = persistCombineReducers(persistConfig, {
    ...reducers
  });

  const store = createStore(
    rootReducer,
    undefined,
    compose(
      applyMiddleware(thunk),
    )
  );

  const persistor = persistStore(store);

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = rootReducer;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}