import { AsyncStorage } from 'react-native';

export default {
  key: 'root',
  storage: AsyncStorage,
};
