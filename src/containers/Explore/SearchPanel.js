import React, { Component } from "react";
import { View, StyleSheet, Dimensions, Text } from "react-native";
import PropTypes from 'prop-types';
import Touchable from 'react-native-platform-touchable';
import Icon from 'react-native-vector-icons/MaterialIcons';
import StarRating from 'react-native-star-rating';
import ActionSheet from 'react-native-actionsheet'

import { theme } from "../../styles/theme";
import ViewWrapper from '../../components/ViewWrapper';
import { FormSubmitButton } from '../../components/FormSubmitButton';

const { height, width } = Dimensions.get('window');

export default class SearchPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: props.rating || 0,
      distance: props.distance || 0,
      distanceIndex: props.distanceIndex || 0
    }

    this.ActionSheet = null;
  }

  onStarRatingPress = (rating) => {
    this.setState({
      rating
    })
  }

  showActionsheet = () => {
    this.ActionSheet.show();
  }

  onActionSheetSelect = (index) => {
    let distance = 0;
    switch (index) {
      case 0:
        distance = 1;
        break;
      case 1:
        distance = 2;
        break;
      case 2:
        distance = 5;
        break;
      case 3:
        distance = 10;
        break;
      default:
        distance = 0;
        break;
    }

    this.setState({
      distance,
      distanceIndex: index
    })
  }

  applyFilters = () => {
    const { rating, distance, distanceIndex } = this.state;
    const { onApplyFilters, onCloseSearchPanel } = this.props;

    if (rating > 0 || distance > 0) {
      onApplyFilters({
        rating,
        distance,
        distanceIndex
      });
    } else {
      onCloseSearchPanel()
    }
  }

  clearFilters = () => {
    const { onClearFilters, onCloseSearchPanel } = this.props;
    this.setState({
      rating: 0,
      distance: 0
    }, () => { onClearFilters(); onCloseSearchPanel() })

  }

  render = () => {
    const { rating, distance, distanceIndex } = this.state;
    const { onCloseSearchPanel } = this.props;
    return (
      <View style={styles.panel}>
        <ViewWrapper withFade={true} withMove={true} fromBackgroundStyle={styles.wrapperFromBackground}>

          <Touchable onPress={onCloseSearchPanel} style={styles.closeIcon}>
            <Icon name="close" size={25} color={'rgba(100,100,100,0.7)'} />
          </Touchable>

          <Text style={styles.title}>SELECT FILTERS</Text>

          <View style={styles.filterPanel}>

            <View style={styles.itemTitleBox}>
              <Text style={styles.titleSub}>Rating</Text>
            </View>
            <View style={[styles.subInner, { flex: 1 }]}>
              <StarRating
                starSize={20}
                disabled={false}
                maxStars={5}
                rating={rating}
                selectedStar={this.onStarRatingPress}
                fullStarColor={'#00AB1D'}
              />
            </View>
          </View>

          <View style={styles.filterPanel}>
            <View style={styles.itemTitleBox}>
              <Text style={styles.titleSub}>Distance</Text>
            </View>
            <View style={styles.subInner}>
              <Touchable onPress={this.showActionsheet} style={styles.distanceButton}>
                <Text style={{ color: '#FFF' }}>{['Less than 1km', 'Within 2km', 'Within 5km', '10km or more', 'Cancel'][distanceIndex]}</Text>
              </Touchable>
            </View>
          </View>

          <FormSubmitButton onPress={this.applyFilters} title={'APPLY FILTERS'} />

          <Text onPress={this.clearFilters} style={styles.clearButton}>Clear Filters</Text>

          <ActionSheet
            ref={o => this.ActionSheet = o}
            title={'Whats your preferred distance'}
            options={['Less than 1km', 'Within 2km', 'Within 5km', '10km or more', 'Cancel']}
            cancelButtonIndex={4}
            onPress={this.onActionSheetSelect}
          />

        </ViewWrapper>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapperFromBackground: {
    backgroundColor: theme.PRIMARY_BACKGROUND,
    padding: 15
  },
  panel: {
    width,
    height,
    position: 'absolute'
  },
  closeIcon: {
    alignSelf: 'flex-end',
    padding: 5
  },
  title: {
    fontWeight: '600',
    color: '#444',
    marginTop: 12
  },
  filterPanel: {
    flexDirection: 'row',
    height: 40,
    marginTop: 35
  },
  itemTitleBox: {
    width: 100,
    justifyContent: 'center'
  },
  titleSub: {
    color: 'rgba(100,100,100,0.7)'
  },
  subInner: {
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  distanceButton: {
    padding: 5,
    paddingHorizontal: 10,
    backgroundColor: '#00AB1D',
    borderRadius: 20
  },
  clearButton: {
    textAlign: 'center',
    color: 'rgba(180,180,180,1)',
    marginTop: 25
  }
});

SearchPanel.propTypes = {
  rating: PropTypes.number,
  distance: PropTypes.number,
  distanceIndex: PropTypes.number,
  onApplyFilters: PropTypes.func.isRequired,
  onClearFilters: PropTypes.func.isRequired,
  onCloseSearchPanel: PropTypes.func.isRequired
}