import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, StyleSheet } from 'react-native';
import Touchable from 'react-native-platform-touchable';
import { withNavigation } from 'react-navigation';

import { StarRating } from '../../components/StarRating';
import { RestaurentTags } from '../../components/RestaurentTags';


const RestaurantTile = (restaurant) => {
  const { navigation, name, rating, logoUrl, type } = restaurant;
  return (
    <View style={styles.container}>
      <Touchable onPress={() => navigation.push('RestaurantDetails', { restaurant })} style={styles.button}>
        <Fragment>
          <Image style={styles.image} source={{ uri: logoUrl }} />
          <View style={styles.infoBox}>
            <Text style={styles.name}>{name}</Text>
            <StarRating rating={rating} style={styles.rating} />
            <RestaurentTags tags={type} style={styles.tags} />
          </View>
        </Fragment>
      </Touchable>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 120,
    marginHorizontal: 15,
    backgroundColor: '#FFF',
    marginBottom: 10,
    borderTopEndRadius: 5,
    borderBottomEndRadius: 5,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgba(200,200,200,0.3)',
  },
  button: {
    flexDirection: 'row',
    borderRadius: 5,
    overflow: "hidden"
  },
  image: {
    width: 120,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5
  },
  infoBox: {
    flex: 1,
    padding: 15
  },
  name: {
    fontSize: 16,
    fontWeight: '500',
    color: '#444'
  },
  rating: {
    marginVertical: 5
  },
  tags: {
    marginTop: 30
  }
})

export default withNavigation(RestaurantTile);