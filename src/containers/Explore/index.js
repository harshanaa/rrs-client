import React, { Component } from 'react'
import { View, ScrollView, FlatList, StyleSheet, LayoutAnimation, Dimensions, ActivityIndicator } from 'react-native';

import ViewWrapperAsync from '../../components/ViewWrapperAsync';
import MainSearchBox from '../../components/MainSearchBox';
import RestaurantTile from './RestaurantTile';
import SearchPanel from './SearchPanel';
import ListEmptyComp from './ListEmptyComp';

import { theme } from "../../styles/theme";

import restaurantService from '../../services/restaurantService';

const { height } = Dimensions.get('window');

export default class Explore extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      title: 'Explore',
      headerStyle: {
        backgroundColor: "#FD8B3F",
      },
      headerTintColor: theme.PRIMARY_TEXT_INVERT,
    }
  };

  constructor() {
    super();
    this.state = {
      isReady: false,
      searchActive: false,
      isSearchPanelVisible: false,

      isLoading: true,

      near: 0,
      rating: 0,
      distance: 1,
      searchTerm: '',
      distanceIndex: 0
    }
  }

  componentDidMount = async () => {
    await this.getLocation();
    this.updateList();
  }

  getLocation = async () => {
    const position = await this.getCurrentPosition();
    this.setState({
      near: `${position.coords.latitude},${position.coords.longitude}`
    });
  }

  getCurrentPosition = (options = {}) => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  };

  updateList = async () => {
    this.setState({
      isLoading: true,
    });
    setTimeout(async () => {
      try {
        const { rating, distance, searchTerm: type, near } = this.state;
        let search = {};

        if (near) {
          search.near = near;
          search.distance = distance ? distance : 1
        }

        if (rating) {
          search.rating = rating;
        }

        if (type) {
          search.type = type;
        }

        const restaurants = await restaurantService.fetchRestaurants(search);
        this.setState({
          data: restaurants,
          isLoading: false,
          isReady: true
        });
      } catch (e) {
        console.log(e);
      }
    }, 1000);
  }

  onSearch = (string) => {
    this.setState({
      searchTerm: string
    }, () => this.updateList())
  }

  onShowSearchPanel = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeIn);
    this.setState({
      isSearchPanelVisible: true
    })
  }

  onCloseSearchPanel = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeIn);
    this.setState({
      isSearchPanelVisible: false
    })
  }

  onClearFilters = () => {
    this.setState({
      rating: 0,
      distance: 0,
      distanceIndex: 0,
      searchTerm: '',
      searchActive: false
    }, () => this.updateList())
  }

  onApplyFilters = ({ rating, distance, distanceIndex }) => {
    this.onCloseSearchPanel();
    this.setState({
      rating,
      distance,
      distanceIndex,
      searchActive: true
    }, () => this.updateList())
  }

  render = () => {
    const { isReady, searchActive, isSearchPanelVisible, rating, distance, distanceIndex, isLoading, data } = this.state;
    return (
      <ViewWrapperAsync isReady={isReady} withFade={true} withMove={true} fromBackgroundStyle={styles.wrapperFromBackground}>
        <MainSearchBox onShowSearchPanel={this.onShowSearchPanel} searchActive={searchActive} onSubmit={this.onSearch} />
        <ScrollView style={{ flex: 1 }}>
          {
            isLoading ? (
              <View style={styles.loadingContainer}>
                <ActivityIndicator
                  animating
                  color={'rgba(100,100,100,0.4)'}
                  size={'small'}
                />
              </View>
            ) : (
                < FlatList
                  style={{ marginTop: - 15 }}
                  data={[...data]}
                  keyExtractor={(item) => item._id}
                  ListHeaderComponent={() => <View style={{ height: 20 }} />}
                  renderItem={({ item }) => <RestaurantTile {...item} />}
                  ListEmptyComponent={() => <ListEmptyComp />}
                />
              )
          }
        </ScrollView>
        {
          isSearchPanelVisible ? (
            <SearchPanel
              rating={rating}
              distance={distance}
              distanceIndex={distanceIndex}
              onApplyFilters={this.onApplyFilters}
              onClearFilters={this.onClearFilters}
              onCloseSearchPanel={this.onCloseSearchPanel}
            />
          ) : null
        }
      </ViewWrapperAsync>
    )
  }
}

const styles = StyleSheet.create({
  wrapperFromBackground: {
    backgroundColor: theme.PRIMARY_BACKGROUND
  },
  loadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: height - 180
  }
});