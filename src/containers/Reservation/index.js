import React, { Component } from 'react'
import { StyleSheet, SectionList, RefreshControl } from 'react-native';

import ViewWrapperAsync from '../../components/ViewWrapperAsync';
import { theme } from "../../styles/theme";
import { ReservationCard } from './ReservationCard';
import { ListHeader } from './ListHeader';
import { ListEmptyComp } from './ListEmptyComp';

import reservationService from '../../services/reservationService';

export default class Reservation extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      title: 'Reservations',
      headerStyle: {
        backgroundColor: "#FD8B3F",
      },
      headerTintColor: theme.PRIMARY_TEXT_INVERT,
    }
  };

  constructor() {
    super();
    this.state = {
      isReady: false,
      isRefreshing: false,
      data: []
    }
  }

  componentDidMount = async () => {
    this.fetchData();
  }

  fetchData = async () => {
    try {
      this.setState({
        isRefreshing: true
      });
      const reservations = await reservationService.myReservations();
      this.setState({
        data: [...reservations],
        isReady: true,
        isRefreshing: false,
      });
    } catch (e) {
      this.setState({
        data: [],
        isReady: true,
        isRefreshing: false
      });
    }
  }

  onPressCard = (reservationId, restaurant, seats, slot, date, message) => {
    this.props.navigation.push('ReservationEditor',
      {
        action: 'edit',
        restaurant: { ...restaurant[0] },
        reservationId: reservationId,
        seats,
        slot,
        date,
        message
      });
  }

  render = () => {
    return (
      <ViewWrapperAsync isReady={this.state.isReady} withFade={true} withMove={true} fromBackgroundStyle={styles.wrapperFromBackground}>
        <SectionList
          keyExtractor={(item) => item._id}
          sections={this.state.data}
          refreshControl={
            <RefreshControl
              tintColor={'rgba(100,100,100,0.4)'}
              refreshing={this.state.isRefreshing}
              onRefresh={this.fetchData}
            />
          }
          renderItem={({ item }) => <ReservationCard {...item} onPress={this.onPressCard} />}
          renderSectionHeader={({ section }) => <ListHeader title={section._id} />}
          ListEmptyComponent={() => <ListEmptyComp />}
        />
      </ViewWrapperAsync>
    )
  }
}

const styles = StyleSheet.create({
  wrapperFromBackground: {
    backgroundColor: theme.PRIMARY_BACKGROUND
  }
});