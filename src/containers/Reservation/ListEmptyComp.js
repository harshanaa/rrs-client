import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

export const ListEmptyComp = () => {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={require('../../images/explore/sad-face.png')} />
      <Text style={styles.text}>You dont have any reservations.</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height,
    width,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 150
  },
  image: {
    height: 100,
    width: 100,
    opacity: 0.2
  },
  text: {
    marginTop: 20,
    opacity: 0.5
  }
});