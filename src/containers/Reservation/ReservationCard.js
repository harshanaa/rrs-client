import React from "react";
import PropTypes from 'prop-types';
import { View, StyleSheet, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Touchable from 'react-native-platform-touchable';
import moment from 'moment';

export const ReservationCard = ({ onPress, restaurant, seats, slot, date, message, _id }) => {
  return (
    <View style={styles.container}>
      <Touchable onPress={() => onPress(_id, restaurant, seats, slot, date, message)}>
        <View style={styles.innerContainer}>
          <Image style={styles.image} source={{ uri: restaurant[0].logoUrl }} />
          <View style={styles.detailContainer}>

            <Text style={styles.restaurantName}>{restaurant[0].name}</Text>

            <View style={[styles.row, { marginTop: 10 }]}>
              <Icon name="date-range" size={15} color={'rgba(100,100,100,0.8)'} />
              <Text style={styles.subText}>{'  ' + moment(date).format("Do MMM YYYY")}</Text>
            </View>

            <View style={styles.row}>
              <Icon name="access-time" size={15} color={'rgba(100,100,100,0.8)'} />
              <Text style={styles.subText}>{'  ' + moment(slot).format("h.mm A")}</Text>
            </View>

            <View style={styles.row}>
              <Icon name="face" size={15} color={'rgba(100,100,100,0.8)'} />
              <Text style={styles.subText}>  {seats} people</Text>
            </View>
          </View>
        </View>
      </Touchable>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 130,
    backgroundColor: '#FFF',
    marginHorizontal: 15,
    borderRadius: 3,
    marginBottom: 20,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgba(200,200,200,0.3)',
  },
  innerContainer: {
    flexDirection: 'row'
  },
  subText: {
    fontSize: 12,
    color: 'rgba(100,100,100,0.8)'
  },
  image: {
    height: 130,
    width: 130,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5
  },
  detailContainer: {
    flex: 1,
    padding: 10,
    justifyContent: 'space-between'
  },
  restaurantName: {
    color: '#444',
    fontWeight: '600',
    fontSize: 16
  },
  row: {
    flexDirection: 'row'
  }
})

ReservationCard.propTypes = {
  onPress: PropTypes.func.isRequired,
  seats: PropTypes.number.isRequired,
  slot: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  message: PropTypes.string,
  _id: PropTypes.string,
}