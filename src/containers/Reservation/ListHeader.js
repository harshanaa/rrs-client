import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';
import moment from 'moment'

import { theme } from "../../styles/theme";

export const ListHeader = ({ title }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{moment(title).format("Do MMM YYYY")}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(255,255,255,0.9)',
    padding: 15,
    marginBottom: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'rgba(200,200,200,0.3)',
  },
  text: {
    color: theme.PRIMARY_TEXT_COLOR,
    fontWeight: '600'
  }
})

ListHeader.propTypes = {
  title: PropTypes.string.isRequired,
}