import React, { Component } from "react";
import { View } from "react-native";

import ViewWrapperAsync from '../../components/ViewWrapperAsync';

export default class RestaurentDetails extends Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isReady: true
      })
    }, 1000);
  }

  render = () => {
    return (
      <ViewWrapperAsync isReady={this.state.isReady} withFade={true} withMove={true} fromBackgroundStyle={styles.wrapperFromBackground}>

      </ViewWrapperAsync>
    )
  }
}