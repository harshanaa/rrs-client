import React, { Component } from 'react'
import { Animated, Dimensions, Easing, Text, StyleSheet, ScrollView } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { isEmpty } from 'lodash';
import Validator from 'validator';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { FormTextInput } from '../../components/FormTextInput';
import { FormPasswordInput } from '../../components/FormPasswordInput';
import { FormSubmitButton } from '../../components/FormSubmitButton';
import { theme } from "../../styles/theme";
import { login } from '../../actions/authActions';

const { height } = Dimensions.get('window');

const mapDispatchToProps = (dispatch) => {
  return {
    login: bindActionCreators(login, dispatch),
  }
}

@connect(null, mapDispatchToProps)
export default class Login extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  });

  constructor() {
    super();
    this.state = {
      scaleAnimation: new Animated.Value(0),
      moveAnimation: new Animated.Value(0),
      fadeAnimation: new Animated.Value(0),
      rotateAnimation: new Animated.Value(0.5),
      username: '',
      password: '',
      errors: {}
    }
  }

  componentDidMount = () => {
    this.playMoveAnimation();
  }

  moveAnimation = (reverse = false) => {
    return Animated.timing(this.state.moveAnimation, {
      toValue: reverse ? 0 : 1,
      duration: 600,
      easing: Easing.in(),
      useNativeDriver: true
    })
  }

  fadeAnimation = () => {
    return Animated.timing(this.state.fadeAnimation, {
      toValue: 1,
      duration: 150,
      easing: Easing.linear,
      useNativeDriver: true
    })
  }

  scaleAnimation = () => {
    return Animated.timing(this.state.scaleAnimation, {
      toValue: 10,
      duration: 400,
      easing: Easing.linear,
      useNativeDriver: true
    })
  }

  rotateAnimation = () => {
    return Animated.loop(
      Animated.sequence([
        Animated.timing(this.state.rotateAnimation, {
          toValue: 1,
          duration: 400,
          easing: Easing.inOut(Easing.quad),
          useNativeDriver: true
        }),
        Animated.timing(this.state.rotateAnimation, {
          toValue: 0,
          duration: 400,
          easing: Easing.inOut(Easing.quad),
          useNativeDriver: true
        })
      ])
    )
  }

  playMoveAnimation = (reverse = false) => {
    this.moveAnimation(reverse).start();
  }

  validateInput = ({ username, password }) => {
    const errors = {};

    if (Validator.isEmpty(username)) {
      errors.username = `Email is a required field.`;
    } else if (!Validator.isEmail(username)) {
      errors.username = `Email looks invalid.`
    }

    if (Validator.isEmpty(password)) {
      errors.password = `Password is a required field.`
    }

    return {
      errors,
      isValid: isEmpty(errors)
    };
  }

  isValid = () => {
    const { errors, isValid } = this.validateInput(this.state);
    if (!isValid) {
      this.setState({ errors });
    }
    return isValid;
  }

  playSignInAnimation = () => {
    this.playMoveAnimation(true);
    setTimeout(() => {
      this.rotateAnimation().start();
    }, 600)
  }

  resetSignInAnimation = () => {
    this.rotateAnimation().stop();
    this.playMoveAnimation(false);
  }

  resetToHome = () => {
    this.scaleAnimation().start();
    setTimeout(() => {
      this.fadeAnimation().start();
      setTimeout(() => {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Home' })],
        });
        this.props.navigation.dispatch(resetAction);
      }, 100);
    }, 200)
  }

  signIn = async () => {
    if (this.isValid()) {
      const { username, password } = this.state;
      try {
        this.playSignInAnimation();
        await this.props.login(username.trim(), password.trim());
        setTimeout(() => {
          this.resetToHome();
        }, 1000);
      } catch (e) {
        this.setState({
          errors: {
            server: e.response.errors ? e.response.errors[0].msg : 'Something went wrong'
          }
        });
        setTimeout(() => {
          this.resetSignInAnimation();
        }, 1000);
      }
    }
  }

  createAccount = () => {
    this.props.navigation.push('Signup');
  }

  render = () => {
    const { errors, username, password } = this.state;
    return (
      <ScrollView style={styles.container} >
        <Animated.View style={[styles.bubble, {
          opacity: this.state.fadeAnimation.interpolate({ inputRange: [0, 1], outputRange: [1, 0] }),
          transform: [
            { translateY: this.state.moveAnimation.interpolate({ inputRange: [0, 1], outputRange: [(height / 2 - 60), height / 6] }) },
            { scale: this.state.scaleAnimation.interpolate({ inputRange: [0, 10], outputRange: [1, 10] }) }
          ]
        }]} />

        <Animated.View style={[styles.logoOuter, {
          transform: [
            { translateY: this.state.moveAnimation.interpolate({ inputRange: [0, 1], outputRange: [(height / 2 - 60), height / 6] }) }
          ]
        }]}>
          <Animated.Image style={[styles.logo, {
            opacity: this.state.fadeAnimation.interpolate({ inputRange: [0, 1], outputRange: [1, 0] }),
            transform: [{
              rotate: this.state.rotateAnimation.interpolate({
                inputRange: [0, 0.5, 1],
                outputRange: ['-10deg', '0deg', '10deg']
              })
            }]
          }]} source={require('../../images/init/logo-2.png')} />
        </Animated.View>

        <Animated.View style={[styles.fieldsContainer, {
          transform: [{
            translateY: this.state.moveAnimation.interpolate({ inputRange: [0, 1], outputRange: [60, 0] }),
          }],
          opacity: this.state.moveAnimation.interpolate({ inputRange: [0, 1], outputRange: [0, 1] })
        }]}>

          <FormTextInput placeholder={'Email'} value={username} onChangeText={username => this.setState({ username })} icon={'users'} containerStyle={styles.username} />
          <FormPasswordInput placeholder={'Password'} value={password} onChangeText={password => this.setState({ password })} containerStyle={styles.password} />
          <Text style={styles.errors}>{Object.values(errors)[0]}</Text>

          <FormSubmitButton onPress={this.signIn} title={'SIGN IN'} />

          <Text onPress={this.createAccount} style={styles.noAccount}>Dont have an account ? <Text style={styles.noAccountButton}>Click here</Text></Text>

        </Animated.View>
      </ScrollView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.PRIMARY_BACKGROUND
  },
  bubble: {
    zIndex: -1,
    height: 120,
    width: 120,
    borderRadius: 100,
    backgroundColor: theme.PRIMARY_BACKGROUND_DARK,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    position: 'absolute',
  },
  logoOuter: {
    height: 120,
    width: 120,
    position: 'absolute',
    alignSelf: 'center',
  },
  logo: {
    height: 120,
    width: 120,
    position: 'absolute',
    alignSelf: 'center',
    zIndex: 5,
  },
  fieldsContainer: {
    zIndex: -1,
    marginTop: height / 2.4,
  },
  username: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'rgba(200,200,200,0.4)',
    borderTopEndRadius: 5,
    borderTopStartRadius: 5
  },
  password: {
    borderBottomEndRadius: 5,
    borderBottomStartRadius: 5
  },
  errors: {
    color: '#F7484C',
    textAlign: 'center',
    paddingTop: 20,
    fontSize: 13,
    fontWeight: '600'
  },
  noAccount: {
    textAlign: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 13,
    fontWeight: '400',
    color: 'rgba(180,180,180,1)'
  },
  noAccountButton: {
    textDecorationLine: 'underline'
  }
});