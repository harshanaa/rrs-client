import React from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import { theme } from "../styles/theme";

import Init from './Init';
import Login from './Login';
import Signup from './Signup';
import Explore from './Explore';
import Reservation from './Reservation';
import RestaurantDetails from './RestaurantDetails';
import ReservationEditor from './ReservationEditor';

const ExploreNavigator = createStackNavigator({
  explore: { screen: Explore },
  RestaurantDetails: { screen: RestaurantDetails },
  ReservationEditor: { screen: ReservationEditor }
});

const ReservationsNavigator = createStackNavigator({
  reservation: { screen: Reservation },
  ReservationEditor: { screen: ReservationEditor }
});

const MainTabNavigator = createBottomTabNavigator({
  explore: {
    screen: ExploreNavigator,
    navigationOptions: {
      tabBarIcon: ({ focused }) => <Icon name="bowl" size={25} color={focused ? theme.TAB_ACTIVE : theme.TAB_INACTIVE} />,
    }
  },
  reservations: {
    screen: ReservationsNavigator,
    navigationOptions: {
      tabBarIcon: ({ focused }) => <Icon name="calendar" size={25} color={focused ? theme.TAB_ACTIVE : theme.TAB_INACTIVE} />,
    }
  }
}, {
    tabBarOptions: {
      showLabel: false,
      style: {
        borderTopWidth: StyleSheet.hairlineWidth,
        backgroundColor: '#FFF',
        borderTopColor: 'rgba(200,200,200,0.3)',
      },
    },
  });

export const AppNavigator = createStackNavigator({
  Init: { screen: Init },
  Home: { screen: MainTabNavigator },
  Login: { screen: Login },
  Signup: { screen: Signup },
}, {
    initialRouteName: 'Init',
    navigationOptions: {
      header: null,
    }
  });