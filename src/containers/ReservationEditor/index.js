import React, { Component, Fragment } from "react";
import { View, StyleSheet, ScrollView, Text, ActivityIndicator, Alert } from "react-native";
import Touchable from 'react-native-platform-touchable';

import { theme } from "../../styles/theme";
import ViewWrapperAsync from '../../components/ViewWrapperAsync';
import { FormContainer } from '../../components/FormContainer';
import { FormTextField } from '../../components/FormTextField';
import { FormSlotPicker } from '../../components/FormSlotPicker';
import { FormDatePicker } from '../../components/FormDatePicker';
import { FormSubmitButton } from '../../components/FormSubmitButton';
import reservationService from '../../services/reservationService';
import localPushService from '../../services/localPushService';

export default class ReservationEditor extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      title: navigation.state.params.action === "create" ? 'Add Reservation' : 'Edit Reservation',
      headerStyle: {
        backgroundColor: "#FD8B3F",
      },
      headerTintColor: theme.PRIMARY_TEXT_INVERT,
    }
  };

  constructor(props) {
    super(props);
    const {
      seats,
      slot,
      date,
      message
    } = props.navigation.state.params;

    this.state = {
      isReady: false,
      action: props.action,

      checking: null,
      availablity: false,
      disable: false,

      date: date ? new Date(date) : new Date(),
      slot: slot ? slot : props.navigation.state.params.restaurant.serviceTimes[0],
      guest: seats ? seats : 1,
      text: message ? message : ''
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isReady: true
      })
    }, 1000);
  }

  onTimeChanged = (slot) => {
    this.setState({
      slot
    });
  }

  onDateChanged = (date) => {
    this.setState({
      date
    });
  }

  onChangeGuest = (text) => {
    this.setState({
      guest: Number(text)
    });
  }

  onChangeSpecialReq = (text) => {
    this.setState({
      text
    });
  }

  addLocalReminder = (reservationId, name, date, slot) => {
    const _slot = new Date(slot);
    const reservationTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), _slot.getHours(), _slot.getMinutes(), _slot.getSeconds());
    localPushService.scheduleNotification(reservationId, reservationTime, name);
  }

  removeLocalReminder = (reservationId) => {
    localPushService.cancelNotification(reservationId);
  }


  editReservation = async () => {
    this.setState({
      disable: true,
      checking: true,
    });

    const { reservationId, restaurant } = this.props.navigation.state.params;
    const {
      date,
      slot,
      text: message,
      guest: seats
    } = this.state;

    try {
      const reservation = await reservationService.editReservation(reservationId, seats, date.toISOString().substr(0, 10), slot, message);

      this.removeLocalReminder(reservationId);
      setTimeout(() => {
        this.addLocalReminder(reservationId, restaurant.name, date, slot);
      }, 100)

      this.setState({
        disable: false,
        checking: false
      }, () => {
        Alert.alert(
          'Reservation updated!',
          'You can change your reservations later from the reservations tab.',
          [
            { text: 'Okay', onPress: () => this.props.navigation.popToTop() },
          ],
          { cancelable: false }
        )
      });
    } catch (e) {
      alert(e.response.errors ? e.response.errors[0].msg : 'Something went wrong');

      this.setState({
        disable: false,
        checking: false
      });
    }
  }

  makeNewReservation = async () => {
    this.setState({
      disable: true,
      checking: true,
    });

    const {
      _id: restaurantId,
      name
    } = this.props.navigation.state.params.restaurant;
    const {
      date,
      slot,
      text: message,
      guest: seats
    } = this.state;

    try {
      const reservation = await reservationService.addReservation(restaurantId, seats, date.toISOString().substr(0, 10), slot, message);
      this.addLocalReminder(reservation._id, name, date, slot);

      this.setState({
        disable: false,
        checking: false
      }, () => {
        Alert.alert(
          'Reservation added!',
          'You can change your reservations later from the reservations tab.',
          [
            { text: 'Okay', onPress: () => this.props.navigation.popToTop() },
          ],
          { cancelable: false }
        )
      });

    } catch (e) {
      alert(e.response.errors ? e.response.errors[0].msg : 'Something went wrong');

      this.setState({
        disable: false,
        checking: false
      });
    }
  }

  deleteReservation = async () => {
    const { reservationId } = this.props.navigation.state.params;
    try {
      await reservationService.deleteReservation(reservationId);
      this.removeLocalReminder(reservationId);
      Alert.alert(
        'Reservation deleted!',
        'You can make new reservations from Restaurant details.',
        [
          { text: 'Okay', onPress: () => this.props.navigation.popToTop() },
        ],
        { cancelable: false }
      )
    } catch (e) {
      alert(e.response.errors ? e.response.errors[0].msg : 'Something went wrong');
    }
  }

  renderStatus = (checking, availablity) => {
    if (checking === null) { // didnt check yet
      return null
    } else if (checking === true) { // loading icon
      return (<Fragment>
        <ActivityIndicator
          animating
          color={'rgba(100,100,100,0.4)'}
          size={'small'}
          style={[{ opacity: !this.props.isReady ? 1 : 0 }, styles.loader]}
        />
        <Text style={{ color: '#444' }}>  Checking availablity... </Text>
      </Fragment>)
    }
  }

  render = () => {
    const { restaurant, action } = this.props.navigation.state.params;
    const { checking, availablity, date, slot, guest, text } = this.state;

    return (
      <ViewWrapperAsync isReady={this.state.isReady} withFade={true} withMove={true} fromBackgroundStyle={styles.wrapperFromBackground}>
        <ScrollView style={{ paddingTop: 10 }}>
          <FormContainer title={"Restaurant"}>
            <FormTextField disabled={true} value={restaurant.name} placeholder={'Restaurant name...'} onChangeText={() => { }} />
          </FormContainer>

          <FormContainer title={"Date"}>
            <FormDatePicker date={date} onDateChanged={this.onDateChanged} />
          </FormContainer>

          <FormContainer horizontal title={"Slot"}>
            <FormSlotPicker selected={restaurant.serviceTimes.findIndex(x => x === slot)} available={restaurant.serviceTimes} onTimeChanged={this.onTimeChanged} />
          </FormContainer>

          <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: 'rgba(100,100,100,0.2)', paddingTop: 10 }} />

          <FormContainer title={"Guests"}>
            <FormTextField keyboardType='numeric' disabled={false} value={guest.toString()} placeholder={'Number of guests'} onChangeText={this.onChangeGuest} />
          </FormContainer>

          <FormContainer title={"Special request"}>
            <FormTextField multiline disabled={false} value={text} placeholder={'Eg: we need child seats'} onChangeText={this.onChangeSpecialReq} />
          </FormContainer>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 15, marginTop: 20 }}>
            {
              this.renderStatus(checking, availablity)
            }
          </View>

          <FormSubmitButton
            onPress={action === 'create' ? this.makeNewReservation : this.editReservation}
            title={action === 'create' ? "CONFRIM" : "UPDATE"}
            style={{ marginTop: 10 }}
          />

          {
            action === 'edit' ? (
              <Touchable onPress={this.deleteReservation} style={{ padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: theme.DISRUPTIVE_COLOR, fontWeight: '600' }}>Cancel Reservation</Text>
              </Touchable>
            ) : null
          }

          <View style={{ height: 50 }} />

        </ScrollView>
      </ViewWrapperAsync>
    )
  }
}


const styles = StyleSheet.create({
  wrapperFromBackground: {
    backgroundColor: theme.PRIMARY_BACKGROUND
  }
});

