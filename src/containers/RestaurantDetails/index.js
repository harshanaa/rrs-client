import React, { Component } from "react";
import { View, StyleSheet, Image, Text, ScrollView } from "react-native";
import Slideshow from 'react-native-slideshow';
import Icon from 'react-native-vector-icons/Feather';
import moment from 'moment';

import { theme } from "../../styles/theme";
import ViewWrapperAsync from '../../components/ViewWrapperAsync';
import { StarRating } from '../../components/StarRating';
import { RestaurentTags } from '../../components/RestaurentTags';
import { FormSubmitButton } from '../../components/FormSubmitButton'

export default class RestaurantDetails extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      title: 'Details',
      headerStyle: {
        backgroundColor: "#FD8B3F",
      },
      headerTintColor: theme.PRIMARY_TEXT_INVERT,
    }
  };

  constructor() {
    super();
    this.state = {
      isReady: false
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isReady: true
      })
    }, 1000);
  }

  makeNewReservation = () => {
    this.props.navigation.push('ReservationEditor',
      {
        action: 'create',
        restaurant: { ...this.props.navigation.state.params.restaurant }
      });
  }

  render = () => {
    const { restaurant } = this.props.navigation.state.params;
    return (
      <ViewWrapperAsync isReady={this.state.isReady} withFade={true} withMove={true} fromBackgroundStyle={styles.wrapperFromBackground}>
        <ScrollView>
          <Slideshow dataSource={restaurant.images} />

          <View style={styles.logoBox}>
            <Image style={styles.logo} source={{ uri: restaurant.logoUrl }} />
          </View>

          <View style={styles.infoBox}>
            <Text style={styles.name}>{restaurant.name}</Text>
            <StarRating rating={4} style={styles.rating} />
            <Text style={styles.desc}>{restaurant.description}</Text>

            <RestaurentTags tags={restaurant.type} style={styles.tags} />

            <View style={styles.timeBox}>
              <Icon name="clock" size={15} color={'rgba(100,100,100,0.7)'} />
              <Text style={styles.time}>{restaurant.serviceTimes.map(time => moment(time).format("h.mm A")).join("  ")}</Text>
            </View>

            <FormSubmitButton onPress={this.makeNewReservation} title={"MAKE RESERVATION"} style={{ marginTop: 20, marginBottom: 50 }} />

          </View>
        </ScrollView>
      </ViewWrapperAsync>
    )
  }
}


const styles = StyleSheet.create({
  wrapperFromBackground: {
    backgroundColor: theme.PRIMARY_BACKGROUND
  },
  logoBox: {
    position: 'absolute',
    alignSelf: 'center',
    top: 160,
    padding: 5,
    backgroundColor: '#FFF',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgba(200,200,200,0.6)',
    borderRadius: 3
  },
  logo: {
    height: 100,
    width: 100,
  },
  infoBox: {
    marginTop: 65,
    padding: 15
  },
  name: {
    fontSize: 16,
    fontWeight: '500',
    color: '#444',
    textAlign: 'center'
  },
  rating: {
    alignSelf: 'center',
    marginTop: 5
  },
  desc: {
    lineHeight: 17,
    fontSize: 13,
    textAlign: 'center',
    marginTop: 15,
    color: theme.PRIMARY_TEXT_COLOR
  },
  tags: {
    marginTop: 20,
    alignSelf: 'center'
  },
  timeBox: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50
  },
  time: {
    color: 'rgba(100,100,100,0.8)',
    marginLeft: 5
  }
});