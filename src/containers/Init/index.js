import React, { Component } from "react";
import { Text, Animated, Easing, StyleSheet, StatusBar } from "react-native";
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { theme } from "../../styles/theme";
import { validateToken } from '../../actions/authActions';

console.disableYellowBox = true;
StatusBar.setBackgroundColor('#f7f7f7')

const mapStateToProps = (state, ownProps) => {
  return {
    access_token: state.auth.access_token
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    validateToken: bindActionCreators(validateToken, dispatch),
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Init extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  });

  constructor() {
    super();
    this.state = {
      scaleAnimation: new Animated.Value(0),
      fadeAnimation: new Animated.Value(0),
      rotateAnimation: new Animated.Value(0.5),
    }
  }

  componentDidMount = () => {
    this.rotateAnimation().start();
    setTimeout(() => {
      this.validateToken();
    }, 1000);
  }

  validateToken = async () => {
    try {
      const { access_token, validateToken } = this.props;
      await validateToken(access_token);
      this.resetToHome();
    } catch (e) {
      console.log(e)
      this.resetToLogin();
    }
  }

  scaleAnimation = () => {
    return Animated.timing(this.state.scaleAnimation, {
      toValue: 10,
      duration: 400,
      easing: Easing.linear,
      useNativeDriver: true
    })
  }

  fadeAnimation = () => {
    return Animated.timing(this.state.fadeAnimation, {
      toValue: 1,
      duration: 150,
      easing: Easing.linear,
      useNativeDriver: true
    })
  }

  rotateAnimation = () => {
    return Animated.loop(
      Animated.sequence([
        Animated.timing(this.state.rotateAnimation, {
          toValue: 1,
          duration: 400,
          easing: Easing.inOut(Easing.quad),
          useNativeDriver: true
        }),
        Animated.timing(this.state.rotateAnimation, {
          toValue: 0,
          duration: 400,
          easing: Easing.inOut(Easing.quad),
          useNativeDriver: true
        })
      ])
    )
  }

  resetToLogin = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  resetToHome = () => {
    this.scaleAnimation().start();
    setTimeout(() => {
      this.fadeAnimation().start();
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home' })],
      });
      this.props.navigation.dispatch(resetAction);
    }, 250)
  }

  render = () => {
    return (
      <Animated.View style={[styles.container, { opacity: this.state.fadeAnimation.interpolate({ inputRange: [0, 1], outputRange: [1, 0] }) }]} >
        <Animated.View style={[styles.bubble, { transform: [{ scale: this.state.scaleAnimation.interpolate({ inputRange: [0, 10], outputRange: [1, 10] }) }] }]} />
        <Animated.Image style={[styles.logo, {
          opacity: this.state.scaleAnimation.interpolate({ inputRange: [0, 3], outputRange: [1, 0] }),
          transform: [{
            rotate: this.state.rotateAnimation.interpolate({
              inputRange: [0, 0.5, 1],
              outputRange: ['-10deg', '0deg', '10deg']
            })
          }]
        }]} source={require('../../images/init/logo-2.png')} />

        <Animated.Text style={[styles.subtitle, {
          opacity: this.state.scaleAnimation.interpolate({ inputRange: [0, 3], outputRange: [1, 0] }),
        }]}>RRS v1</Animated.Text>
      </Animated.View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F7F7FB',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bubble: {
    height: 120,
    width: 120,
    borderRadius: 100,
    backgroundColor: theme.PRIMARY_BACKGROUND_DARK,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: 120,
    width: 120,
    position: 'absolute',
  },
  subtitle: {
    fontSize: 12,
    fontWeight: '400',
    color: 'rgba(180,180,180,1)',
    position: 'absolute',
    bottom: 20
  }
})