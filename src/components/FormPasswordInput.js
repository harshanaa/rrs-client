import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, StyleSheet, Dimensions, ViewPropTypes, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import { theme } from "../styles/theme";

const { width } = Dimensions.get('window');

export class FormPasswordInput extends Component {
  constructor() {
    super();
    this.state = {
      passwordSafe: true
    }
  }

  onShowPassword = () => {
    this.setState((prevState) => {
      return {
        passwordSafe: !prevState.passwordSafe
      }
    })
  }


  render = () => {
    const { passwordSafe } = this.state;
    const { containerStyle, style, placeholder, onChangeText, icon, iconColor, value } = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        <TextInput underlineColorAndroid={'#FFF'} value={value} secureTextEntry={passwordSafe} onChangeText={onChangeText} style={styles.textInput} placeholder={placeholder} />
        <Icon name="lock" size={25} color={theme.PRIMARY_BACKGROUND_DARK} style={styles.icon} />
        <Icon onPress={this.onShowPassword} name={passwordSafe ? 'lock' : 'unlock'} size={18} color={theme.SECONDARY_TEXT_COLOR} style={styles.lock} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    backgroundColor: theme.PRIMARY_BACKGROUND_WHITE
  },
  textInput: {
    padding: 15,
    paddingLeft: 70,
    fontWeight: '400',
    fontSize: 13
  },
  icon: {
    marginTop: Platform.OS === 'ios' ? 10 : 15,
    marginLeft: 30,
    width: 30,
    position: 'absolute'
  },
  lock: {
    width: 30,
    position: 'absolute',
    left: width - 70,
    top: Platform.OS === 'ios' ? 13 : 20
  }
})

FormPasswordInput.propTypes = {
  containerStyle: ViewPropTypes.style,
  style: ViewPropTypes.style,
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
};