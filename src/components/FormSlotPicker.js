import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text } from 'react-native';
import Touchable from 'react-native-platform-touchable';
import moment from 'moment';

export class FormSlotPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: props.selected,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedIndex: nextProps.selected
    })
  }

  onPress = (slot, i) => {
    this.setState({
      selectedIndex: i
    });
    this.props.onTimeChanged(slot);
  }

  render = () => {
    const { available } = this.props;
    const { selectedIndex } = this.state;
    return (
      <View style={[styles.container]}>
        {
          available.map((slot, i) => {
            return (
              <Touchable key={slot} onPress={() => this.onPress(slot, i)} style={[styles.button, selectedIndex === i ? styles.selected : null]}>
                <Text style={[styles.text, selectedIndex === i ? styles.selectedText : null]}>{moment(slot).format("h.mm A")}</Text>
              </Touchable>
            )
          })
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    flexDirection: 'row',
  },
  button: {
    marginRight: 10,
    height: 40,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D7F3E0',
    borderRadius: 3
  },
  selected: {
    backgroundColor: '#00AB1D'
  },
  text: {
    color: '#00AB1D'
  },
  selectedText: {
    color: '#FFF'
  }
})

FormSlotPicker.propTypes = {
  selected: PropTypes.number.isRequired,
  available: PropTypes.arrayOf(PropTypes.string),
  onTimeChanged: PropTypes.func.isRequired
}