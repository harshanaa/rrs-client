import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, ViewPropTypes } from 'react-native';

export const RestaurentTags = ({ tags, style }) => {
  return (
    <View style={[styles.container, style]}>
      {
        tags.map(tag => <View key={tag} style={styles.tag}><Text style={styles.tagText}>{tag}</Text></View>)
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  tag: {
    padding: 3,
    paddingHorizontal: 7,
    backgroundColor: 'rgba(200,200,200,0.3)',
    borderRadius: 20,
    marginRight: 5
  },
  tagText: {
    fontSize: 10,
    color: 'rgba(100,100,100,0.8)'
  }
})

RestaurentTags.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string),
  style: ViewPropTypes.style,
}