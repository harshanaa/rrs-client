import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet, ViewPropTypes } from 'react-native';
import Touchable from 'react-native-platform-touchable';

import { theme } from "../styles/theme";

export const FormSubmitButton = ({ onPress, title, style }) => {
  return (
    <Touchable onPress={onPress} style={[styles.container, style]}>
      <Text style={styles.text}>{title}</Text>
    </Touchable>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    height: 50,
    backgroundColor: '#FD8B3F',
    margin: 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: theme.PRIMARY_TEXT_INVERT,
    fontWeight: '700',
    fontSize: 13
  }
})

FormSubmitButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  style: ViewPropTypes.style
}