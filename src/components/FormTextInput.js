import React from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, StyleSheet, ViewPropTypes, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import { theme } from "../styles/theme";

export const FormTextInput = ({ onChangeText, icon, iconColor = theme.PRIMARY_BACKGROUND_DARK, containerStyle, placeholder, value, style }) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <TextInput underlineColorAndroid={'#FFF'} value={value} autoCapitalize={'none'} onChangeText={onChangeText} style={[styles.input, style]} placeholder={placeholder} />
      <Icon name={icon} size={25} color={iconColor} style={styles.icon} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    backgroundColor: theme.PRIMARY_BACKGROUND_WHITE
  },
  input: {
    padding: 15,
    paddingLeft: 70,
    fontWeight: '400',
    fontSize: 13,
  },
  icon: {
    marginTop: Platform.OS === 'ios' ? 10 : 15,
    marginLeft: 30,
    width: 30,
    position: 'absolute'
  }
})

FormTextInput.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  icon: PropTypes.string.isRequired,
  iconColor: PropTypes.string,
  containerStyle: ViewPropTypes.style,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  style: ViewPropTypes.style,
}
