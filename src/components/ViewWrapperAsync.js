import React, { Component } from 'react';
import {
  Text,
  View,
  Animated,
  StyleSheet,
  StatusBar,
  InteractionManager,
  ViewPropTypes,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import { theme } from "../styles/theme";

const { height, width } = Dimensions.get('window');
export default class ViewWrapperAsync extends React.PureComponent {

  static defaultProps = {
    fromBackgroundStyle: {},
    toBackgroundStyle: {},
    withMove: true,
    withFade: true,
    statusBarColor: theme.PRIMARY_STATUS_BAR_COLOR,
    loaderColor: theme.HINT_COLOR
  };

  static propTypes = {
    fromBackgroundStyle: ViewPropTypes.style,
    toBackgroundStyle: ViewPropTypes.style,
    isReady: PropTypes.bool.isRequired,
    withMove: PropTypes.bool,
    withFade: PropTypes.bool,
    statusBarColor: PropTypes.string,
    loaderColor: PropTypes.string
  };

  constructor() {
    super();
    this.state = {
      isTransitionOver: false,
      fadeAnimation: new Animated.Value(0),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isReady && nextProps.isReady) {
      this.startAnimation();
    }
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        isTransitionOver: true
      });
    });
  }

  startAnimation() {
    Animated.timing(
      this.state.fadeAnimation,
      {
        toValue: 1,
        duration: 250,
        useNativeDriver: true
      },
    ).start();
  }

  render() {
    return (
      <View style={[styles.container, this.props.fromBackgroundStyle]}>
        <StatusBar backgroundColor={this.props.statusBarColor} animated={true} />
        {this.state.isTransitionOver ? (
          <Animated.View
            style={[styles.animatedView, this.props.toBackgroundStyle, {
              opacity: this.props.withFade ? this.state.fadeAnimation : 1,
              transform: [
                {
                  translateY: this.props.withMove ? this.state.fadeAnimation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [-5, 0],
                  }) : 1
                }
              ]
            }]}
          >
            {this.props.children}
          </Animated.View>
        ) : (
            null
          )}
        {
          !this.props.isReady ? (
            <ActivityIndicator
              animating
              color={'rgba(100,100,100,0.4)'}
              size={'small'}
              style={[{ opacity: !this.props.isReady ? 1 : 0 }, styles.loader]}
            />
          ) : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: -5,
    backgroundColor: '#FFF'
  },
  animatedView: {
    flex: 1
  },
  loader: {
    position: 'absolute',
    alignSelf: 'center',
    top: (height / 2) - 65
  }
});