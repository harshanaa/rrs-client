import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { theme } from "../styles/theme";

export const FormContainer = ({ title, children, horizontal }) => {
  return (
    <View>
      <View style={styles.titleBox}>
        <Text style={styles.title}>{title}</Text>
      </View>
      <View style={{ flexDirection: horizontal ? 'row' : 'column' }}>
        {children}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  titleBox: {
    paddingLeft: 15,
    height: 35,
    justifyContent: 'center',
  },
  title: {
    color: 'rgba(100,100,100,0.8)'
  },
});


FormContainer.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  horizontal: PropTypes.bool,
};

