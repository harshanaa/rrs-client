import React, { Component, Fragment } from 'react';
import { StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import Touchable from 'react-native-platform-touchable';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

import { theme } from "../styles/theme";

export class FormDatePicker extends Component {

  constructor() {
    super();
    this.state = {
      datepickerVisible: false,
    };
  }

  onSelectDate = (date) => {
    this.hideDatePicker();
    this.props.onDateChanged(date);
  }

  hideDatePicker = () => {
    this.setState({
      datepickerVisible: false,
    });
  }

  showDatePicker = () => {
    this.setState({
      datepickerVisible: true,
    });
  }

  render() {
    const { props } = this;
    return (
      <Touchable onPress={this.showDatePicker} style={styles.container}>
        <Fragment>
          <Text style={styles.text}>{moment(props.date).format('Do MMM Y')}</Text>
          <DateTimePicker
            mode={'date'}
            date={props.date}
            isVisible={this.state.datepickerVisible}
            onConfirm={this.onSelectDate}
            onCancel={this.hideDatePicker}
          />
        </Fragment>
      </Touchable>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    paddingLeft: 5,
    paddingVertical: 15,
    borderBottomColor: 'rgba(100,100,100,0.2)',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  text: {
    paddingLeft: 10,
    color: theme.PRIMARY_TEXT_COLOR
  },
});

FormDatePicker.propTypes = {
  date: PropTypes.instanceOf(Date).isRequired,
  onDateChanged: PropTypes.func.isRequired,
};
