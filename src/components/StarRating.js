import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ViewPropTypes } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export const StarRating = ({ rating, style }) => {

  const colored = rating <= 5 ? rating : 5;
  const uncolored = 5 - rating;

  return (
    <View style={[styles.container, style]}>
      {
        [...Array(colored)].map((e, i) => <Icon key={Math.random().toString()} name={'star'} size={12} color={'#00AB1D'} />)
      }
      {
        [...Array(uncolored)].map((e, i) => <Icon key={Math.random().toString()} name={'star'} size={12} color={'rgba(100,100,100,0.4)'} />)
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  }
})

StarRating.propTypes = {
  rating: PropTypes.number.isRequired,
  style: ViewPropTypes.style,
}