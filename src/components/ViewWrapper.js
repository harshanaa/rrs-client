import React, { PureComponent } from 'react';
import {
  View,
  Animated,
  StyleSheet,
  InteractionManager,
  StatusBar,
  ViewPropTypes
} from 'react-native';
import PropTypes from 'prop-types';

import { theme } from "../styles/theme";

export default class ViewWrapper extends PureComponent {
  constructor() {
    super();
    this.state = {
      isTransitionOver: false,
      fadeAnimation: new Animated.Value(0),
    };
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        isTransitionOver: true,
      });
      this.startAnimation();
    });
  }


  startAnimation() {
    Animated.timing(
      this.state.fadeAnimation,
      {
        toValue: 1,
        duration: 400,
        useNativeDriver: true,
      },
    ).start();
  }

  render() {
    return (
      <View style={[styles.container, this.props.fromBackgroundStyle]}>
        <StatusBar backgroundColor={this.props.statusBarColor} animated />
        {this.state.isTransitionOver ? (
          <Animated.View
            style={[styles.container, this.props.toBackgroundStyle, {
              opacity: this.props.withFade ? this.state.fadeAnimation : 1,
              transform: [
                { translateY: this.props.withMove ? this.state.fadeAnimation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [-5, 0],
                }) : 1 },
              ],
            }]}
          >
            {this.props.children}
          </Animated.View>
        ) : (
          null
        )}
      </View>
    );
  }
}

ViewWrapper.propTypes = {
  fromBackgroundStyle: ViewPropTypes.style,
  toBackgroundStyle: ViewPropTypes.style,
  withMove: PropTypes.bool,
  withFade: PropTypes.bool,
  statusBarColor: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]).isRequired,
};

ViewWrapper.defaultProps = {
  fromBackgroundStyle: { backgroundColor: '#FFF' },
  toBackgroundStyle: {},
  withMove: true,
  withFade: true,
  statusBarColor: theme.PRIMARY_STATUS_BAR_COLOR,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});