import React, { Component } from "react";
import PropTypes from 'prop-types';
import { View, TextInput, StyleSheet } from "react-native";
import Touchable from 'react-native-platform-touchable';
import Icon from 'react-native-vector-icons/Feather';

import { theme } from "../styles/theme";

export default class MainSearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      string: props.value || ''
    }
  }

  onChangeText = (string) => {
    this.setState({
      string
    });
  }

  searchSubmit = () => {
    this.props.onSubmit(this.state.string);
  }

  render = () => {
    const { searchActive, onShowSearchPanel } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput underlineColorAndroid={'transparent'} onSubmitEditing={this.searchSubmit} onChangeText={this.onChangeText} value={this.state.string} style={styles.input} placeholder={"Search tags"} />
        </View>
        <Touchable onPress={onShowSearchPanel} style={styles.filterButton}>
          <View>
            <Icon name="filter" size={25} color={'rgba(100,100,100,0.7)'} />
            <View style={[styles.filterActive, { opacity: searchActive ? 1 : 0 }]} />
          </View>
        </Touchable>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 55,
    backgroundColor: '#FFF',
    margin: 15,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgba(200,200,200,0.3)',
    flexDirection: 'row'
  },
  inputContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  input: {
    fontSize: 16,
    padding: 10,
    paddingLeft: 15
  },
  filterButton: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  filterActive: {
    position: 'absolute',
    height: 14,
    width: 14,
    backgroundColor: '#00AB1D',
    borderRadius: 7,
    borderWidth: 2,
    borderColor: '#FFF',
    top: -2,
    right: -1
  }
})

MainSearchBox.propTypes = {
  onShowSearchPanel: PropTypes.func.isRequired,
  searchActive: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired
}