import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, StyleSheet, Text } from 'react-native';

import { theme } from "../styles/theme";
const minInputHeight = 30;

export class FormTextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textInputHeight: 32,
    };
  }

  onContentSizeChange = (e) => {
    this.setState({
      textInputHeight: e.nativeEvent.contentSize.height > minInputHeight ? e.nativeEvent.contentSize.height : minInputHeight,
    });
  }

  render() {
    const { props } = this;
    return (
      <View style={[styles.container, { backgroundColor: props.disabled ? '#F8f8f8' : '#FFF' }, this.props.error ? styles.errorContainer : null,]}>
        <TextInput
          {...props}
          editable={!props.disabled}
          style={[{ height: this.state.textInputHeight }, styles.textInput]}
          onContentSizeChange={this.onContentSizeChange}
          underlineColorAndroid={'transparent'}
        />
        {
          this.props.error ? (
            <Text style={styles.errorText}>{this.props.error}</Text>
          ) : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    padding: 7,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomColor: 'rgba(100,100,100,0.2)',
    borderBottomWidth: StyleSheet.hairlineWidth,
    width: '100%',
  },
  textInput: {
    color: theme.PRIMARY_TEXT_COLOR,
    fontSize: 14,
  },
  errorContainer: {
    borderColor: theme.DISRUPTIVE_COLOR,
    borderWidth: 1,
    borderBottomColor: theme.DISRUPTIVE_COLOR,
    backgroundColor: 'rgba(255,220,220,0.3)',
    borderBottomWidth: 1
  },
  errorText: {
    color: theme.DISRUPTIVE_COLOR,
    fontWeight: '500',
    marginTop: -3,
    fontSize: 10
  }
});

FormTextField.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  keyboardType: PropTypes.string
}