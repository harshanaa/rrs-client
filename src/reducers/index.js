import {combineReducers} from 'redux'
import auth from './authReducer';

const rootReducer = {
  auth
};

export default rootReducer;