const initialState = {
  access_token: null,
  username: null,
  firstName: null,
  lastName: null
};

function authReducer(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      const { access_token, username, firstName, lastName } = action.payload;
      return { ...state, access_token, username, firstName, lastName };
    case 'LOGOUT_SUCCESS':
      return initialState;
    default:
      return state;
  }
}

export default authReducer;
