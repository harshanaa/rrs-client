
import authService from '../services/authService';
import { setAuthToken } from '../helpers/ApiHelper';

/**
 * Used to login
 * @param {string} username
 * @param {string} password
 */
export const login = (username, password) => {
  return async dispatch => {
    const user = await authService.login(username, password);
    dispatch({
      type: 'LOGIN_SUCCESS',
      payload: user
    });
    setAuthToken(user.access_token);
  };
}

/**
 * Used to signup
 * @param {string} username
 * @param {string} password
 * @param {string} firstName
 * @param {string} lastName
 */
export const signup = (username, password, firstName, lastName) => {
  return async dispatch => {
    const registration = await authService.signup(username, password, firstName, lastName);
    await dispatch(login(username, password));
  };
}



/**
 * validate exsisting token
 * @param {string} access_token
 */
export const validateToken = (access_token) => {
  return async dispatch => {
    const isValid = await authService.isTokenValid(access_token);
    setAuthToken(access_token);
  };
}