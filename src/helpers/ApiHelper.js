
import { BASE_URL } from '../constants';

const API_ADDRESS = `${BASE_URL}:8080`;

let fetchDefaultOptions = {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
  }
};

let currentStore = null;

export const initApi = (store) => {
  currentStore = store;
}

export const setAuthToken = (token) => {
  fetchDefaultOptions.headers = {
    ...fetchDefaultOptions.headers,
    "Authorization": `Bearer ${token}`
  }
}

const _fetch = async (address, fetchOptions) => {
  const fetchOpts = { ...fetchDefaultOptions, ...fetchOptions };
  const response = await fetch(API_ADDRESS + address, fetchOpts);

  if (response.ok) {
    return await response.json();
  } else {
    // TODO: update token here
    const data = await response.json();
    const error = new Error(`${response.status}`);
    error.response = data;
    throw error;
  }
}

export default _fetch;