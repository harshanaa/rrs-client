import fetch from '../helpers/ApiHelper';

const fetchRestaurants = ({ near, distance, type, rating }) => {
  const fetchOptions = {
    method: "GET",
  };
  let query = `/restaurants?`;
  if (near) {
    query = query + `near=${near}&`;
    if (distance) {
      query = query + `distance=${distance}&`;
    } else {
      query = query + `distance=${1}&`;
    }
  }
  if (type) {
    query = query + `type=${type}&`;
  }
  if (rating) {
    query = query + `rating=${rating}&`;
  }
  return fetch(query, fetchOptions);
}

export default {
  fetchRestaurants
}