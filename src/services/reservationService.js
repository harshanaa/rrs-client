import fetch from '../helpers/ApiHelper';

const addReservation = (restaurantId, seats, date, slot, message) => {
  const fetchOptions = {
    method: "POST",
    body: JSON.stringify({
      restaurantId,
      seats,
      date,
      slot,
      message
    })
  };

  return fetch('/reservation', fetchOptions);
}

const editReservation = (reservationId, seats, date, slot, message) => {
  const fetchOptions = {
    method: "POST",
    body: JSON.stringify({
      seats,
      date,
      slot,
      message
    })
  };

  return fetch(`/reservation/${reservationId}`, fetchOptions);
}

const myReservations = () => {
  const fetchOptions = {
    method: "GET",
  };
  return fetch('/reservation/mine', fetchOptions);
}

const deleteReservation = (reservationId) => {
  const fetchOptions = {
    method: "DELETE",
  };
  return fetch(`/reservation/${reservationId}`, fetchOptions);
}

export default {
  addReservation,
  myReservations,
  editReservation,
  deleteReservation
}