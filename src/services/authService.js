import fetch from '../helpers/ApiHelper';

const login = (username, password) => {
  const fetchOptions = {
    method: "POST",
    body: JSON.stringify({
      username,
      password
    })
  };

  return fetch('/login', fetchOptions);
}

const signup = (username, password, firstName, lastName) => {
  const fetchOptions = {
    method: "POST",
    body: JSON.stringify({
      username,
      password,
      firstName,
      lastName
    })
  };

  return fetch('/signup', fetchOptions);
}

const isTokenValid = (token) => {
  const fetchOptions = {
    method: "GET",
    headers: {
      "Authorization": `Bearer ${token}`
    }
  };

  return fetch('/', fetchOptions);
}


export default {
  login,
  signup,
  isTokenValid
}