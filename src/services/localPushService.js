import * as PushNotification from 'react-native-push-notification';
import moment from 'moment';

export const scheduleNotification = (reservationId, date, name) => {
  const _date = moment(date).subtract(30, 'minutes').toDate();
  PushNotification.localNotificationSchedule({
    id: '' + reservationId,
    userInfo: {
      id: '' + reservationId,
    },
    number: 0,
    message: `Your reservation at ${name} is coming up in 15 mins`,
    date: _date
  });
}

export const cancelNotification = (reservationId) => {
  PushNotification.cancelLocalNotifications({ id: '' + reservationId });
}


export default {
  scheduleNotification,
  cancelNotification
}