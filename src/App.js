import React, { Component } from 'react';
import { Provider } from 'react-redux';
import moment from 'moment';
import tz from 'moment-timezone';

import { AppNavigator } from './containers/RootNavigator';
import configureStore from './store/configureStore';
import { initApi } from './helpers/ApiHelper';

// initialize moment
moment.tz.setDefault("Asia/Colombo");

// initialize redux store
export const store = configureStore();

// initialize api wrapper
initApi(store);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}
export default App;